package quiz;
import java.util.Map;

/**問題全体のインタフェース
 * @author 200217AM
 *
 */
public interface Question {

	public Map<String,Integer> create();
}
