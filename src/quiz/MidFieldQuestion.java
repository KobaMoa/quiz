package quiz;

import java.util.LinkedHashMap;
import java.util.Map;

public class MidFieldQuestion implements Question{
	/* 中盤の問題を生成するメソッド
	 * @see quiz.Question#create()
	 */
	public Map<String,Integer> create(){
		Map<String,Integer> arrays=new LinkedHashMap<>();
		arrays.put("クラス名 変数名 = new クラス名(); これは何を生成している？\n1:フィールド 2:メソッド 3:インスタンス 4:コンストラクタ",3);
		arrays.put("クラス型をフィールドとして用いることは可能？\n1:可能 2:不可能",2);
		arrays.put("他のクラスを継承する際に追加される宣言はどれ？\n1:public 2:new 3:SuperHero 4:extends",4);
		arrays.put("基本的にクラスへ継承してもよいクラスは何個まで？\n1:1個 2:2個 3:3個 4:4個",1);
		arrays.put("privateが指定されたフィールドの値を取り出す際に使われるメソッドを通称で何という？\n1:setter 2:getter 3:batter 4:selecter",2);
		return arrays;
	}
}
