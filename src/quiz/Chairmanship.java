package quiz;
import java.util.Map;

public class Chairmanship {

	private int numberOfCorrect;
	private int questionRange;

	/**問題の範囲を指定するメソッド
	 * @return
	 */
	public Question selectQuestion(){
		while(true){
			System.out.println("クイズの範囲を選択してください。\n1:1章～6章　2:7章～13章　3:14章～17章");
			int questionRange=new java.util.Scanner(System.in).nextInt();
			if(questionRange==1){
				Question question=new EarlyStageQuestion();
				return question;
			}else if(questionRange==2){
				Question question=new MidFieldQuestion();
				return question;
			}else if(questionRange==3){
				Question question=new FinalStageQuestion();
				return question;
			}else{
				System.out.println("選びなおしてください。");
				continue;
			}
		}
	}
	/**出題するクイズを生成するメソッド
	 * @param question
	 * @return
	 */
	public Map<String,Integer> createQuestion(Question question){
		Map<String,Integer> arrays=question.create();
		return arrays;
	}
	/**クイズを行うメソッド
	 * @param arrays
	 */
	public void sayQuestion(Map<String,Integer> arrays){
		for(String q:arrays.keySet()){
			int correct=arrays.get(q);
			System.out.println(q);
			int answer=new java.util.Scanner(System.in).nextInt();
			if(answer==correct){
				System.out.println("正解です！");
				numberOfCorrect++;
			}else{
				System.out.println("不正解です…");
			}
		}
		System.out.println("あなたの正解数は"+arrays.size()+"問中"+this.numberOfCorrect+"問です！");
	}
	public int getQuestionRange(){
		return this.questionRange;
	}
	public void setQuestionRange(int num){
		this.questionRange=num;
	}
	public int numberOfCorrect(){
		return this.numberOfCorrect;
	}
	public void setNumbarOfCorrect(int num){
		this.numberOfCorrect=num;
	}
}
