package quiz;

import java.util.LinkedHashMap;
import java.util.Map;

public class EarlyStageQuestion implements Question{
	/* 序盤の問題を生成するメソッド
	 * @see quiz.Question#create()
	 */
	public Map<String,Integer> create(){
		Map<String,Integer> arrays=new LinkedHashMap<>();
		arrays.put
		("次のデータ型のうち、文字の並びを入れる際に用いられるものはどれ？\n1:int 2:String 3:char 4:Hero",2);
		arrays.put
		("プログラムの動作中に中身が書き変わらない変数をなんという？\n1:定数 2:フィールド 3:オペランド 4:メソッド",1);
		arrays.put
		("強制的な型変換を行う際に利用する演算子を何演算子という？？\n1:代入演算子 2:文字列結合演算子 3:キャスト演算子 4:ダブル演算子",3);
		arrays.put
		("変数の値を1だけ減らす--と表記される演算子は次のうちどれ？\n1:デフォールト 2:インクリメント 3:オペランド 4:デクリメント",4);
		arrays.put
		("条件式が「変数==変数」や「変数==値」のみしか使わず、小数や真偽値が使用できない分岐構文はどれ？\n1:if 2:switch 3:if-else 4:while",2);
		return arrays;
	}
}
