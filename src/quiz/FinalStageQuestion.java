package quiz;

import java.util.LinkedHashMap;
import java.util.Map;

public class FinalStageQuestion implements Question{
	/* 終盤の問題を生成するメソッド
	 * @see quiz.Question#create()
	 */
	public Map<String,Integer> create(){
		Map<String,Integer> arrays=new LinkedHashMap<>();
		arrays.put
		("全クラスの祖先にあたりどんなインスタンスでも代入できるクラスはどれ？\n1:UltraHero 2:String 3:Object 4:System",3);
		arrays.put
		("静的フィールドを作るには宣言の際、先頭にどれを付け足せばよい？\n1:package private 2:public 3:final 4:static",4);
		arrays.put
		("大量の文字列を連結する際に使用されるクラスはどれ？\n1:StringBuilder 2:Append 3:Object 4:World",1);
		arrays.put
		("日付や時間を扱う際にJava8から新しく追加されたものはどれ？\n1:Date 2:Time API 3:Calendar 4:Timer",2);
		arrays.put
		("過去の総理大臣の名前と任期を順番に格納するのに適したコレクションはどれ？\n1:HushMap 2:LinkedHashSet 3:LinkedHashMap 4:ArrayList",3);
		return arrays;
	}
}
